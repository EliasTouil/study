/**
Product of all but current index

Given an array of integers, return a new array such that each element at index i of the new array is the product of all the numbers in the original array except the one at i.

Follow-up: what if you can't use division?
*/

const product_of_all_but_i = (num_array) => {
  /* with division
  return num_array.map((val, i) => {
    let result = [...num_array].reduce((a,b) => (a*b));
    return result/val
  });
  */

  //no division O(n)=n+2n^n
  /*
  return num_array.map((val, i) => {
    let arr = [...num_array]; // n*n
    arr.splice(i,1); // n
    let result = arr.reduce((a,b) => (a*b)); // n*n
    return result
  });
  */


  //solution
  //https://www.geeksforgeeks.org/a-product-array-puzzle/

  let n = 5;
  let left = new Array(n);
  let right = new Array(n);
  let result = new Array(n);
  left[0] = 1;
  right[n-1] = 1;

  for (let i = 1; i < n; i++){
    left[i] = num_array[i - 1] * left[i - 1]; //n
  }
  for (let j = n - 2; j >= 0; j--){
    right[j] = num_array[j + 1] * right[j + 1]; //n
  }
  for (let i = 0; i < n; i++){
    result[i] = left[i] * right[i]; //n 
  }
  return result;
}


module.exports = {
  product_of_all_but_i,
}
