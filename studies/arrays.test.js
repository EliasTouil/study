import timeFunction from '../utils/timeFunction';
import { product_of_all_but_i } from './arrays.js'
import { log_perf } from '../utils/perf-logger';
/*
ex 1
For example, if our input was [1, 2, 3, 4, 5], the expected output would be [120, 60, 40, 30, 24]. If our input was [3, 2, 1], the expected output would be [2, 3, 6].
*/
test('Product of all but index of array', async () => {
  jest.setTimeout(20000);
  expect(product_of_all_but_i([1,2,3,4,5])).toEqual([120, 60, 40, 30, 24]);
  console.log(await log_perf(product_of_all_but_i,[4,287,33,4554,5451]));
});
