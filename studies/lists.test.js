import timeFunction from '../utils/timeFunction';
import { log_perf } from '../utils/perf-logger';
import { n_houses_with_k_colors } from './lists.js';
/*
A builder is looking to build a row of N houses that can be of K different colors. He has a goal of minimizing cost while ensuring that no two neighboring houses are of the same color.

Given an N by K matrix where the nth row and kth column represents the cost to build the nth house with kth color, return the minimum cost which achieves this goal.

The cost of painting each house with a certain color is represented by a n x 3 cost matrix. For example, costs[0][0] is the cost of painting house 0 with color red; costs[1][2] is the cost of painting house 1 with color green, and so on... Find the minimum cost to paint all houses.
*/
test('Product of all but index of array', async () => {
  jest.setTimeout(20000);

  // cost

  expect(product_of_all_but_i([1,2,3,4,5])).toEqual([120, 60, 40, 30, 24]);
  console.log(await log_perf(product_of_all_but_i,[4,287,33,4554,5451]));
});
