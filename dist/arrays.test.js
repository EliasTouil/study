"use strict";

var _arrays = require("./arrays.js");

/*
ex 1
For example, if our input was [1, 2, 3, 4, 5], the expected output would be [120, 60, 40, 30, 24]. If our input was [3, 2, 1], the expected output would be [2, 3, 6].
*/
expect((0, _arrays.product_of_all_but_i)([1, 2, 3, 4, 5]).toBe([120, 60, 40, 30, 24]));