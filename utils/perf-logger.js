import fs from 'fs';
import path from 'path';
import { promisify } from 'util';
import { performance } from 'perf_hooks';


/*
Takes a function and its arguments to run a time test and log results in a json file named after the function.
Outputs the new log
*/


const TIME_STRING_FORMAT = {
  year: 'numeric',
  month: 'numeric',
  day: 'numeric',
  hour: '2-digit',
  minute: '2-digit',
  second: '2-digit',
}


const timer = (func, args) => {
  return new Promise((resolve, reject) => {
    let number_of_runs = 1000;
    let total_runtime = 0;

    for( let i=0; i<number_of_runs; i++){
      let t0 = performance.now();
      func(args);
      let t1 = performance.now();
      total_runtime += t1 - t0;
    }

    resolve(total_runtime / number_of_runs);
  });
}


const create_log = (func, args) => {
  return new Promise(async (resolve, reject) => {
    let runtime = 0;

    try {
      runtime = await timer(func, args);
    } catch(err){
      console.error(err);
      reject("perf_logger: timer did not return a runtime")
    }

    if(runtime){
      let now = new Date();
      let now_string = new Intl.DateTimeFormat('en-CAN', TIME_STRING_FORMAT)
          .format(now)
          .replace(/([^a-z0-9]+)/gi, '_');
      resolve({
          [now_string]:{
          date : now,
          runtime: runtime
        }
      });
    }
  });
}


const read_past_logs = (func_name) => {
  let file_name = "perf_logs/" + func_name + ".json";
  return promisify(fs.readFile)(file_name, 'utf8')
    .then(res => JSON.parse(res));
}


const write_log = (func_name, log) => {
  let file_name = "perf_logs/" + func_name + ".json";
  return promisify(fs.writeFile)(file_name, JSON.stringify(log))
}


const log_perf = (func, args) => {
  return new Promise( async (resolve, reject) => {
    let new_log = {};
    let past_logs = {};
    let concat_log = {};

    try {
      new_log = await create_log(func, args);
    } catch (err){
      console.error(err);
      reject("perf_logger: could not create new log object")
    }

    try {
      past_logs = await read_past_logs(func.name);
    } catch (err){
      console.warn(err);
      console.warn("perf_logger: There are no existing log, will try to create  a new one");
    }

    if (Object.getOwnPropertyNames(past_logs).length) {

      concat_log = Object.assign(past_logs, new_log);
      try {
        write_log(func.name, concat_log)
      } catch(err) {
        console.error(err);
        reject("perf_logger: could not write a concatenated log to file");
      }

    } else {

      try {
        write_log(func.name, new_log)
      } catch(err) {
        console.error(err);
        reject("perf_logger: could not write a new log to file");
      }

      console.warn("perf_logger: New log file created");
    }

    //new log can be logged by callee
    resolve(new_log);

  });
}


export {
  log_perf
}
