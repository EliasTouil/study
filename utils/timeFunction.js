const timeFunction = (func, args) => {
  let t0 = performance.now();
  func(args);
  let t1 = performance.now();
  return t1-t0
}

export default timeFunction;
